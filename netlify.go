package main

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

// BaseURL of the Netlify API
const BaseURL = "https://api.netlify.com/api/v1/"

func netlifyRequest(request string) (io.Reader, error) {
	var res *http.Response
	var err error

	client, req := buildClient(request)

	if res, err = client.Do(req); err != nil {
		log.Fatalf("error calling api: %s", err)
	}

	if res.StatusCode >= 200 && res.StatusCode < 300 {
		// str := stringifyResponse(res)
		// logOutputString(str)

		return res.Body, nil
	}

	return nil, errors.New("unable to perform api request")
}

func getToken() string {
	token := os.Getenv("NETLIFY_ACCESS_TOKEN")

	if token == "" {
		log.Fatalf("no token for api requests")
	}

	return fmt.Sprintf("?access_token=%s", token)
}

func buildClient(request string) (*http.Client, *http.Request) {
	var req *http.Request
	var err error

	requestWithToken := fmt.Sprintf("%s%s", request, getToken())

	client := &http.Client{}
	apiURL := fmt.Sprintf("%s%s", BaseURL, requestWithToken)

	if req, err = http.NewRequest("GET", apiURL, nil); err != nil {
		log.Fatalf("unable to create api request: %s", err)
	}

	return client, req
}

func stringifyResponse(res *http.Response) string {
	if body, err := ioutil.ReadAll(res.Body); err != nil {
		log.Fatalf("invalid response")
	} else {
		return string(body)
	}

	return ""
}
