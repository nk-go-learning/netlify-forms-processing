package main

import (
	"log"
	"os"
	"strconv"
	"time"

	_ "github.com/joho/godotenv/autoload"
)

const storeFileName = "db/store.json"
const forwarderFileName = "db/forwarders.json"

func main() {
	// Set polling interval
	intervalDur := setPollingInterval()

	// Get Store
	store := getStore()

	// Get Mailer
	mailer := getMailer()

	dailyReportDestination := getEnvVar("DAILY_REPORT_DESTINATION", "")
	dailyReport := NewDailyReport(intervalDur.Seconds(), dailyReportDestination, mailer)

	for {
		log.Println("Executing Netlify form processing...")

		processForms(store, mailer, dailyReport)

		log.Println("Processing complete, next poll in", intervalDur)
		time.Sleep(intervalDur)
	}
}

func getEnvVar(env string, defaultValue string) string {
	value := os.Getenv(env)

	if value == "" {
		return defaultValue
	}

	return value
}

func setPollingInterval() time.Duration {
	var interval int
	var err error

	intervalStr := getEnvVar("POLL_INTERVAL", "1800")

	if interval, err = strconv.Atoi(intervalStr); err != nil {
		interval = 1800
		log.Println("error converting supplied interval, setting interval to default of", 1800)
	}

	return time.Duration(interval) * time.Second
}

func getStore() *FileSystemStore {
	dbFile, err := os.OpenFile(storeFileName, os.O_RDWR|os.O_CREATE, 0666)

	if err != nil {
		log.Fatalf("problem opening %s %v", storeFileName, err)
	}

	forwarderFile, err := os.OpenFile(forwarderFileName, os.O_RDWR, 0666)

	if err != nil {
		log.Fatalf("problem opening %s %v", forwarderFileName, err)
	}

	store, err := NewFileSystemStore(dbFile, forwarderFile)

	if err != nil {
		log.Fatalf("problem reading store %v", err)
	}

	return store
}

func getMailer() *Mailer {
	host := getEnvVar("EMAIL_HOST", "")
	port, _ := strconv.Atoi(getEnvVar("EMAIL_PORT", ""))
	user := getEnvVar("EMAIL_USER", "")
	pass := getEnvVar("EMAIL_PASS", "")
	from := getEnvVar("EMAIL_FROM", "")

	return NewMailer(host, port, user, pass, from)
}
