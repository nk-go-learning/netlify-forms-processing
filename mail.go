package main

import (
	"fmt"

	"gopkg.in/gomail.v2"
)

// Mailer struct for sending mail
type Mailer struct {
	Host        string
	Port        int
	User        string
	Password    string
	FromAddress string
	D           *gomail.Dialer
}

// NewMailer creates a new mailer
func NewMailer(host string, port int, user, password, from string) *Mailer {
	mailer := &Mailer{
		Host:        host,
		Port:        port,
		User:        user,
		Password:    password,
		FromAddress: from,
	}

	mailer.D = gomail.NewDialer(mailer.Host, mailer.Port, mailer.User, mailer.Password)

	return mailer
}

// NewMail creates a new mail ready for sending
func (m *Mailer) NewMail(siteName, formName string, submission NetlifySubmission, forwarder Forwarder) *gomail.Message {

	subject := buildFormSubject(siteName, formName)
	body := buildFormEmailBody(submission, formName)

	n := gomail.NewMessage()
	n.SetHeader("From", m.FromAddress)
	n.SetHeader("To", forwarder.To...)
	n.SetHeader("Cc", forwarder.Cc...)
	n.SetHeader("Bcc", forwarder.Bcc...)
	n.SetHeader("Subject", subject)
	n.SetBody("text/html", body)

	return n
}

// SendMail creates a new mail ready for sending
func (m *Mailer) SendMail(mail *gomail.Message) error {
	if err := m.D.DialAndSend(mail); err != nil {
		return err
	}

	return nil
}

// NewDailyReportMail creates the daily report mail
func (m *Mailer) NewDailyReportMail(processedCount int, destinationAddress string) *gomail.Message {

	subject := "Netlify Forms Daily Report"
	body := fmt.Sprintf("%d forms were processed in the last 24 hours.", processedCount)

	n := gomail.NewMessage()
	n.SetHeader("From", m.FromAddress)
	n.SetHeader("To", destinationAddress)
	n.SetHeader("Subject", subject)
	n.SetBody("text/html", body)

	return n
}

func buildFormSubject(siteName, formName string) string {
	return fmt.Sprintf("%s new %s submission", siteName, formName)
}

func buildFormEmailBody(submission NetlifySubmission, formName string) string {
	intro := fmt.Sprintf("<p>A new submission for your %s form. Details:</p>", formName)
	var body string

	for _, f := range submission.Fields {
		body += fmt.Sprintf("<p><b>%s</b>: %s</p>", f.Title, f.Value)
	}

	createdAt := fmt.Sprintf("<p>Form was sent at: %s</p>", submission.CreatedAt)

	return fmt.Sprintf("%s%s%s", intro, body, createdAt)
}
