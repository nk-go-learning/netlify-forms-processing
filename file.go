package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
)

// FileSystemStore struct
type FileSystemStore struct {
	store      *json.Encoder
	existing   KnownSubmissions
	forwarders ForwarderMap
}

// AddToExisting adds a processed form to the in memory store
func (f *FileSystemStore) AddToExisting(formID string) {
	f.existing[formID] = true
}

// NewFileSystemStore returns a new FileSystemStore
func NewFileSystemStore(existingFile *os.File, forwarderFile *os.File) (*FileSystemStore, error) {
	err := initialiseStoreFile(existingFile)

	if err != nil {
		return nil, fmt.Errorf("problem initialising store file, %v", err)
	}

	forwarders := readForwarders(forwarderFile)
	existingSubmissions, err := readExistingForms(existingFile)

	if err != nil {
		return nil, fmt.Errorf("problem loading existing submissions from file %s, %v", existingFile.Name(), err)
	}

	return &FileSystemStore{
		store:      json.NewEncoder(&Tape{existingFile}),
		existing:   existingSubmissions,
		forwarders: forwarders,
	}, nil
}

func initialiseStoreFile(file *os.File) error {
	file.Seek(0, 0)

	info, err := file.Stat()

	if err != nil {
		return fmt.Errorf("problem getting file info from file %s, %v", file.Name(), err)
	}

	if info.Size() == 0 {
		file.Write([]byte("{}"))
		file.Seek(0, 0)
	}

	return nil
}

func readExistingForms(rdr io.Reader) (KnownSubmissions, error) {
	var known KnownSubmissions

	err := json.NewDecoder(rdr).Decode(&known)
	if err != nil {
		err = fmt.Errorf("problem parsing site submissions, %v", err)
	}

	return known, err
}

func readForwarders(rdr io.Reader) ForwarderMap {
	var forwarders ForwarderMap

	err := json.NewDecoder(rdr).Decode(&forwarders)

	if err != nil || len(forwarders) == 0 {
		log.Fatalf("cannot run without forwarders set: %s", err)
	}

	return forwarders
}
