# Netlify Forms Processsing

A small Go program that is designed to run in the background and process Netlify form submissions on a set schedule. The program will check the Netlify API once every hour and it will:

* Get a list of your sites
* For each site with forms, it will get all submissions
* Compare each submission to any forms it knows about and;
    * Send an email with the form data if it hasn't seen that form before
    * Skip this form if it's already sent an email
* Write the results to a `.json` file

## How to Run

You'll need to set the environment variable `NETLIFY_ACCESS_TOKEN` with an access token from your Netlify account. Then start the go program and it will run in the background.

## Todo

|Feature|Status|
|-------|------|
|Make schedule configurable|Done|
|Make the email credentials configurable|Done|
|Easily configure destination email addresses|Done|
|Alert on site submissions status getting close to the limit|Pending|
