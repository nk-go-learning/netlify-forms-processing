package main

import (
	"encoding/json"
	"log"
	"strings"
)

const (
	sitesEndpoint       = "sites"
	formsEndpoint       = "sites/{site_id}/forms"
	submissionsEndpoint = "submissions"
)

func processForms(store *FileSystemStore, mailer *Mailer, dailyReport *DailyReport) {
	// Get all known sites from Netlify
	sites := getSites()

	// Get all known forms from Netlify
	forms := getForms(sites)

	// Get all known submissions from Netlify
	submissions := getSubmissions()

	// Store our gathered Netlify data
	netlify := NetlifyData{
		sites,
		forms,
		submissions,
	}

	// Process each submission
	processedCount, processed := checkAndSendForms(store, netlify, mailer)

	// Add processed count to daily report
	dailyReport.CheckDailyReport(processedCount)

	// Write the processed submissions to the db
	store.store.Encode(processed)
}

// getSites will get all sites from Netlify for the currently provided access
// token.
func getSites() Sites {
	var netlifySites []NetlifySite
	sites := make(Sites)

	if body, err := netlifyRequest(sitesEndpoint); err != nil {
		log.Printf("unable to complete %s request: %s", sitesEndpoint, err)
	} else {
		err := json.NewDecoder(body).Decode(&netlifySites)

		if err != nil {
			log.Printf("error with json decoder: %v", err)
			return nil
		}

		for _, s := range netlifySites {
			sites[s.SiteID] = NetlifySite{
				SiteID: s.SiteID,
				Name:   s.Name,
				Forms:  make(map[string]NetlifyForm),
			}
		}

		return sites
	}

	return nil
}

// getForms will get all forms for the currently provided access token. It will
// also map each site to a form for easy lookup in later processing.
func getForms(sites Sites) Forms {
	var forms []NetlifyForm
	formsMap := make(Forms)

	for i := range sites {
		siteFormUrl := strings.Replace(formsEndpoint, "{site_id}", sites[i].SiteID, 1)

		if body, err := netlifyRequest(siteFormUrl); err != nil {
			log.Printf("unable to complete %s request: %s", formsEndpoint, err)
		} else {
			err := json.NewDecoder(body).Decode(&forms)

			if err != nil {
				log.Printf("error with json decoder: %v", err)
				return nil
			}

			for _, f := range forms {
				formsMap[f.ID] = f
			}
		}
	}

	formsMap.MapFormsToSites(sites)
	return formsMap
}

// getSubmissions will get all submissions known to the currently provided
// access token.
func getSubmissions() []NetlifySubmission {
	var submissions []NetlifySubmission

	// TODO: Make this respect pagination

	if body, err := netlifyRequest(submissionsEndpoint); err != nil {
		log.Printf("unable to complete %s request: %s", formsEndpoint, err)
	} else {
		err := json.NewDecoder(body).Decode(&submissions)

		if err != nil {
			log.Printf("error with json decoder: %v", err)
			return nil
		}
	}

	log.Printf("Netlify returned %d submissions.", len(submissions))

	return submissions
}

func checkAndSendForms(store *FileSystemStore, data NetlifyData, mailer *Mailer) (int, KnownSubmissions) {
	processedCount := 0
	processed := make(KnownSubmissions)

	// TODO: Possibly make this concurrent?
	for i, s := range data.Submissions {
		_, ok := store.existing[s.ID]

		if !ok {
			form := data.Forms[s.FormID]

			forwarder, ok := store.forwarders[form.Site.Name]

			if !ok {
				log.Println("Unable to find forwarding details for site:", form.Site.Name)
				continue
			}

			m := mailer.NewMail(form.Site.Name, form.Name, s, forwarder)

			log.Println("Sending new email for submission", form.Site.Name, s.ID, forwarder.To)

			err := mailer.SendMail(m)
			if err != nil {
				log.Printf("Failed to send mail: %s, will attempt again in next cycle", err)
				continue
			}

			s.Emailed = true
			data.Submissions[i] = s
			processedCount++

			processed[s.ID] = true
			store.AddToExisting(s.ID)
		}
	}

	// Add the already existing processed submissions to our new map of
	// processed submissions
	for k, v := range store.existing {
		processed[k] = v
	}

	log.Println("Processed", processedCount, "forms in this cycle.")

	return processedCount, processed
}
