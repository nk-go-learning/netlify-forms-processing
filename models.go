package main

// NetlifyData struct contains all the data returned from Netlify
type NetlifyData struct {
	Sites       Sites
	Forms       Forms
	Submissions []NetlifySubmission
}

// Sites map
type Sites map[string]NetlifySite

// NetlifySite struct
type NetlifySite struct {
	SiteID string `json:"id"`
	Name   string `json:"name"`
	Forms  map[string]NetlifyForm
}

// Forms map which contains all forms keyed by ID
type Forms map[string]NetlifyForm

// MapFormsToSites adds the site struct to each form
func (f Forms) MapFormsToSites(sites Sites) {
	for key, form := range f {
		form.Site = sites[form.SiteID]
		f[key] = form
	}
}

// NetlifyForm struct returned from 'forms' endpoint
type NetlifyForm struct {
	SiteID           string `json:"site_id"`
	ID               string `json:"id"`
	Name             string `json:"name"`
	SubmissionCount  int64  `json:"submission_count"`
	LastSubmissionAt string `json:"last_submission_at"`
	Submissions      []NetlifySubmission
	Site             NetlifySite
}

// NetlifySubmission struct returned from 'submissions' endpoint
type NetlifySubmission struct {
	ID        string      `json:"id"`
	FormID    string      `json:"form_id"`
	Fields    []FormField `json:"ordered_human_fields"`
	CreatedAt string      `json:"created_at"`
	Emailed   bool        `json:"emailed"`
}

// FormField struct returned field from 'submissions' endpoint
type FormField struct {
	Title string `json:"title"`
	Name  string `json:"name"`
	Value string `json:"value"` // TODO: this probably isn't always a string
}

// KnownSubmissions is a map of known submissions that we have processed and stored
type KnownSubmissions map[string]bool

// ForwarderMap is a map of Forwarders keyed on site name
type ForwarderMap map[string]Forwarder

// Forwarder struct that stores where we need to forward the submissions to
type Forwarder struct {
	To  []string `json:"to"`
	Cc  []string `json:"cc"`
	Bcc []string `json:"bcc"`
}
