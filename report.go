package main

import "log"

// DailyReport struct
type DailyReport struct {
	currentProcessCount   int
	maxProcessCountPerDay int
	totalProcessedForms   int
	mailer                *Mailer
	destinationEmail      string
}

// NewDailyReport creates a DailyReport struct
func NewDailyReport(interval float64, email string, mailer *Mailer) *DailyReport {

	const oneDay = 86400
	count := int(oneDay / interval)

	return &DailyReport{
		currentProcessCount:   0,
		maxProcessCountPerDay: count,
		totalProcessedForms:   0,
		mailer:                mailer,
		destinationEmail:      email,
	}
}

// CheckDailyReport checks the daily report and will send an email if required
func (d *DailyReport) CheckDailyReport(processedCount int) {
	d.totalProcessedForms += processedCount
	d.currentProcessCount++

	// TODO: remove this when we're comfortable it's working
	log.Println("Daily report updated, current process count:", d.currentProcessCount, "out of:", d.maxProcessCountPerDay)

	if d.currentProcessCount == d.maxProcessCountPerDay {

		if d.destinationEmail != "" {
			mail := d.mailer.NewDailyReportMail(d.totalProcessedForms, d.destinationEmail)
			d.mailer.SendMail(mail)
		} else {
			log.Println("Daily report destination not specified, nothing sent.")
		}

		// Reset processed count
		d.reset()
	}
}

func (d *DailyReport) reset() {
	d.currentProcessCount = 0
	d.totalProcessedForms = 0
}
