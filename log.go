package main

import "log"

// LogOutput logs responses to console
const LogOutput = true

// LogOutputString logs response strings to console
const LogOutputString = false

func logOutput(toLog string) {
	if LogOutput == true {
		log.Println(toLog)
	}
}

func logOutputString(toLog string) {
	if LogOutputString == true {
		log.Println(toLog)
	}
}
